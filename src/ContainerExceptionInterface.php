<?php
/**
 * PHP FIG
 *
 * @category     PSR-11
 * @package      Container
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\Container;

/**
 * Base interface representing a generic exception in a container.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ContainerExceptionInterface extends Psr\Container\ContainerExceptionInterface {

}
