<?php
/**
 * PHP FIG
 *
 * @category     PSR-11
 * @package      Container
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\Container;

/**
 * No entry was found in the container.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface NotFoundExceptionInterface extends ContainerExceptionInterface {

}
